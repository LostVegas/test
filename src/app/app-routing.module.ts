import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoreComponent } from './components/store/store.component';
import { CartComponent } from './components/cart/cart.component';
import { OrderComponent } from './components/order/order.component';


const routes: Routes = [
  {
    path: 'store',
    component: StoreComponent
  },
  {
    path: 'cart',
    component: CartComponent
  },
  {
    path: 'cart/order',
    component: OrderComponent
  },
  { path: '', redirectTo: 'store', pathMatch: 'full'},
  { path: '**', redirectTo: 'store'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
