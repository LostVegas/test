import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  data;
  productList;

  constructor(private dataService: DataService,
              private cartService: CartService) { }

  ngOnInit() {
    this.dataService.getData().subscribe(data => {
      this.data = data;
    });
    this.cartService.getProductList().subscribe(el => this.productList = el);
  }

  removeItem(el) {
    this.cartService.removeProduct(el);
  }

  removeAll() {
    this.productList = [];
    this.cartService.clearCart();
  }

}
