import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  isEditable = true;
  isChecked = false;
  productList;
  isLoaded = false;
  isFinished = false;
  checkedOption: number;

  constructor(private formBuilder: FormBuilder,
              private cartService: CartService) { }

  ngOnInit() {
    this.firstFormGroup = this.formBuilder.group({
      id: [null],
      userName: ['', Validators.required],
      userPhone: ['', [Validators.required, Validators.pattern(/^[0-9]+(?!.)/)]]
    });
    this.thirdFormGroup = this.formBuilder.group({
      userSnils: ['', Validators.required],
      userPhoto: ['', Validators.required]
    });
    this.fourthFormGroup = this.formBuilder.group({
      userAddress: ['', Validators.required],
      userIndex: ['', Validators.required]
    });

    this.cartService.getProductList().subscribe(el => this.productList = el);
  }

  getData() {
    console.log(this.firstFormGroup);
  }

  order() {
    this.isLoaded = true;
    setTimeout(() => {
      this.isLoaded = false;
      this.isFinished = true;
      this.productList = [];
      this.cartService.clearCart();
    }, 2000);
  }

  selectOption(id) {
    this.checkedOption = id;
  }

  changeCheckbox(ev) {
    this.isChecked = ev.checked;
  }

}
