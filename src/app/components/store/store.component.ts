import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit, OnDestroy {

  data;

  constructor(private dataService: DataService,
              private cartService: CartService) { }

  ngOnInit() {
    this.dataService.getData().subscribe(data => {
      this.data = data;
    });
  }

  addToCart(el) {
    this.cartService.add(el);
  }

  ngOnDestroy() {
    // this.dataService.unsubscribe();
  }

}
