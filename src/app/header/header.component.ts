import { Component, OnInit, DoCheck } from '@angular/core';
import { CartService } from '../services/cart.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, DoCheck {

  productsCount: number;

  subject = new Subject();

  constructor(private cartService: CartService) { }

  ngOnInit() {
    this.cartService.getProductList().subscribe(el => {
      this.productsCount = el.length;
      this.subject.next(this.productsCount);
    });
  }

  ngDoCheck() {
    this.cartService.getProductList().subscribe(el => {
      this.productsCount = el.length;
      this.subject.next(this.productsCount);
    });
  }

}
