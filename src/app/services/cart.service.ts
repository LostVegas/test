import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  productList = [];
  productListStorage = JSON.parse(localStorage.getItem('productList'));

  constructor() {
    if (this.productListStorage != null) {
      this.productList = this.productListStorage;
    }
  }

  add(el) {
    this.productList.push(el);
    this.addToLocalStorage(this.productList);
    return this.productList;
  }

  addToLocalStorage(el) {
    localStorage.removeItem('productList');
    localStorage.setItem('productList', JSON.stringify(el));
  }

  removeProduct(el) {
    let index = this.productList.indexOf(el);
    if (index > -1) {
      this.productList.splice(index, 1);
      localStorage.removeItem('productList');
      localStorage.setItem('productList', JSON.stringify(this.productList));
    }
  }

  getProductList(): Observable<any> {
    return of(this.productList);
  }

  clearCart() {
    localStorage.removeItem('productList');
    this.productList = [];
  }
}
